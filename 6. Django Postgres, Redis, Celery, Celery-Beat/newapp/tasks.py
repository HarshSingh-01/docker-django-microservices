from __future__ import absolute_import, unicode_literals
import celery
from celery import shared_task


@shared_task
def add(x, y):
    return x + y


@shared_task(name="printMessageTask")
def printMessage(message, secondarg):
    print(message)
    print(secondarg)
    return message, secondarg
