from django.shortcuts import render
from django.http.response import HttpResponse
from django_celery_beat.models import PeriodicTask, CrontabSchedule
from newapp.tasks import printMessage
import datetime
import json
from django.contrib.auth import get_user_model

# Create your views here.


def index(request):
    time = datetime.datetime.now()
    # '''
    scheduled, created = CrontabSchedule.objects.get_or_create(
        hour="19", minute="02", timezone='Asia/Kolkata')
    # if created:
    task = PeriodicTask.objects.create(
        crontab=scheduled, name='print message' + str(time), task='printMessageTask', one_off=True, args=json.dumps(
            ["task scheduled at " + str(time), "second argument"]
        ))

    print(type('print message' + str(time)))
    # '''
    users = get_user_model().objects.all()
    date_joined = {}
    for user in users:
        print("Object type : ", type(user.date_joined))
        print("User joined time: ", user.date_joined)
        date_joined["year"] = user.date_joined.year
        date_joined["month"] = user.date_joined.month
        date_joined["day"] = user.date_joined.day
        date_joined["hour"] = user.date_joined.hour
        date_joined["minute"] = user.date_joined.minute
        date_joined["second"] = user.date_joined.second

    print("date joined: ", date_joined)

    print("current time type: ", type(time))
    print("Current time: ", time)
    return HttpResponse("Hello")


def task_info(request):
    task_object = PeriodicTask.objects.filter(
        name="print message2023-04-28 15:05:14.587236")
    for task in task_object:
        print(task.id)
        print(task.name)
        print(task.task)
        print(task.args)

    task.delete()
    return HttpResponse("Task info")
    pass
