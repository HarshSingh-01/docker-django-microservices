from django.urls import path, include
from . import views

urlpatterns = [
    path('check/', views.index, name='index'),
    path('taskinfo/', views.task_info, name='taskinfo')
]
