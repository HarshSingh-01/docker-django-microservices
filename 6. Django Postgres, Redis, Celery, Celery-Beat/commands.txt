# Checking the configurations of docker compose file
docker compose config

# Building docker images using docker compose
docker compose build

# Starting django project and app

    # Apline Build
    docker compose run app sh -c "django-admin startproject django_project ."
    docker compose run app sh -c "django-admin startapp newapp"

    # Ubutnu build
    docker compose run --rm app django-admin startproject django_project . 
    docker compose run --rm app django-admin startapp newapp

# Celery task verification

    # Ubuntu
    docker compose exec -it <django_app>(newapp) /bin/bash

    # Activate interactive Console
    python manage.py shell

    # import task
    from newapp.tasks import add
    a = add.delay(3,4)
    a.result

# Start containers
docker compose up


# Run the django-app VM

    # For Alpine Build
    docker exec -it django_container sh

    # For Ubuntu Build
    docker exec -it django_container /bin/bash