import datetime

scheduled = datetime.timedelta(days=365, hours=5, minutes=1)

now = datetime.datetime.now()
print("Current date and time : ", now)
print("Scheduled date and time: ", scheduled)
