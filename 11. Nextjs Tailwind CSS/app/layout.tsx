import './globals.css'
import { Nunito } from 'next/font/google'

export const metadata = {
  title: 'Nextjs',
  description: 'Container for Nextjs',
}

const font = Nunito({
  subsets: ["cyrillic-ext"]
})

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={font.className}>{children}</body>
    </html>
  )
}
