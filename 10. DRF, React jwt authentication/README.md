# Docker Commands

## Build Container

```sh
docker compose build
```

## Run Container

```sh
docker compose up
```

## Installing a module inside container individually

These commands will run the container and stop after installing the package.

### Backend

```sh
docker compose run --rm backend pip install python-package
```

### Frontend

```sh
docker compose run --rm frontend npm install node-package
```

Alternate solution is that you can paste the package name in the requirements.txt, package.json for backend and frontend.
