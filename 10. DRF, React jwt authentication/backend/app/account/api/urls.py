from django.urls import path
from . import views
from .views import GetRoutesView, MyTokenObtainPairView, StatusCheckView

from rest_framework_simplejwt.views import (
    TokenRefreshView,
)

urlpatterns = [
    path('', GetRoutesView.as_view()),
    path('token/', MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('notes/', StatusCheckView.as_view(), name='Status Check View'),
]
