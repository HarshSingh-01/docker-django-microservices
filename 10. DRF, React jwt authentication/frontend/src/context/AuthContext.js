import { createContext, useState, useEffect } from 'react'
import jwt_decode from "jwt-decode";
import { useNavigate } from 'react-router-dom'
import { AuthManager } from './AuthManager';

const AuthContext = createContext()

export default AuthContext;


export const AuthProvider = ({children}) => {
    const [contextData, loading] = AuthManager();

    return(
        <AuthContext.Provider value={contextData} >
            {loading ? null : children}
        </AuthContext.Provider>
    )
}
